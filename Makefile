all:
	tar --extract --verbose -f rogue5.4.2-src.tar.gz --one-top-level=rogue5.4.2-src

	swig -java -outdir app/src/main/java/ main.i
	gcc -fpic -c rogue5.4.2-src/main.c main_wrap.c -I/home/stefan/.jdks/adopt-openj9-16/include/ -I/home/stefan/.jdks/adopt-openj9-16/include/linux
	gcc -shared main.o main_wrap.o -o libmain.so

	swig -java -outdir app/src/main/java/ mdport.i
	gcc -fpic -c rogue5.4.2-src/mdport.c mdport_wrap.c -I/home/stefan/.jdks/adopt-openj9-16/include/ -I/home/stefan/.jdks/adopt-openj9-16/include/linux
	gcc -shared mdport.o mdport_wrap.o -o libmdport.so

	javac JRogue.java

clean:
	rm --force *.o
	rm --force *_wrap.c
	rm --force app/src/main/java/*JNI.java
	rm --force libmain.so
	rm --force app/src/main/java/Main.java
#  rm --force app/src/main/java/MdPort.java


