%module Main

extern void endit(int sig);

extern void fatal(char *s);

extern int rnd(int range);


extern int roll(int number, int sides);


%{#ifdef SIGTSTP %}
extern void tstp(int ignored);
%{#endif %}

extern playit();


extern void leave(int sig);

extern shell();

extern void my_exit(int st);


