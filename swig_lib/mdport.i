%module MdPort

extern void md_init();

extern int md_hasclreol();

extern int md_putchar(int c);

extern int md_raw_standout();

extern int md_raw_standend();

extern int md_unlink_open_file(char *file, int inf);

extern int md_unlink(char *file);

extern int md_creat(char *file, int mode);


extern int md_normaluser();

extern int md_getuid();

extern char * md_getusername();

extern char * md_gethomedir();

extern int md_sleep(int s);

extern char * md_getshell();

extern int md_shellescape();

extern int directory_exists(char *dirname);

extern char * md_getroguedir();

extern char * md_getrealname(int uid);

extern char *xcrypt(char *key, char *salt);
extern char * md_crypt(char *key, char *salt);

extern char * md_getpass(prompt);


extern unsigned long int md_ntohl(unsigned long int x);

extern unsigned long int md_htonl(unsigned long int x);


extern int md_readchar();
